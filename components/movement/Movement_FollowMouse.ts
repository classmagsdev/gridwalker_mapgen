
import * as Matter from 'matter-js';
import { GridActor } from '../characters/GridActor';

export class Movement_FollowMouse {

    clickCatcher: createjs.MovieClip;
    maxMovement: number;
    isMoving: boolean;
    movingCharacter: any;
    
    root: any;
    stage: any;
    body: any;

    camLock: createjs.MovieClip;

    playZone: {width: number, height: number};

    constructor({root: root, stage: stage}, playZone, movingCharacter: GridActor, clickCatcher: createjs.MovieClip) {
        this.maxMovement = movingCharacter.speed;
        this.movingCharacter = movingCharacter;
        this.clickCatcher = clickCatcher;

        this.clickCatcher.addEventListener("mousedown", this.startMove.bind(this));
        this.clickCatcher.addEventListener("pressup", this.stopMove.bind(this));

        root.addEventListener("tick", this.moveCharacter.bind(this));

        this.playZone = {width: playZone.width, height: playZone.height};

        this.stage = stage;

        this.camLock = this.movingCharacter.camLock;
        
    }

    startMove() {
        this.isMoving = true;
    }

    stopMove() {
        this.isMoving = false;
    }

    moveCharacter() {
        if (!this.isMoving) return;

        //#region TEST 1

        // This section can move around the entire map freely and accurately (with and without camera movement), but the collisions jump

        let placeMC = this.camLock // alias

        let point = placeMC.localToGlobal(0, 0);

        let deltaX = this.stage.mouseX / this.stage.scaleX - point.x / this.stage.scaleX;
        let deltaY = this.stage.mouseY / this.stage.scaleY - point.y / this.stage.scaleY;
        let distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        
        let percent = Math.min(this.maxMovement / distance, 1);
        
        let chara = this.movingCharacter;
        
        placeMC.x += deltaX * percent;
        placeMC.x = Math.min(Math.max(chara.size.width * .5, placeMC.x), this.playZone.width - chara.size.width * .5);
        placeMC.y += deltaY * percent;
        placeMC.y = Math.min(Math.max(chara.size.width * .5, placeMC.y), this.playZone.height - chara.size.width * .5);

        Matter.Body.setPosition(chara.body, {x: placeMC.x, y: placeMC.y})

        //#endregion

        //#region TEST 2

        // This section works better for the collision handling, but there's weird movement

        // let chara = this.movingCharacter // alias
        // let charaBody = this.movingCharacter.body // alias

        // let point = {x: charaBody.position.x, y: charaBody.position.y};

        // //appears works without camera movement
        // let deltaX = this.stage.mouseX / this.stage.scaleX - point.x;
        // let deltaY = this.stage.mouseY / this.stage.scaleY - point.y;

        // let distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        
        // let percent = Math.min(this.maxMovement / distance, 1);

        // var originalPos = {x: charaBody.position.x, y: charaBody.position.y};

        // originalPos.x += deltaX * percent;
        // originalPos.x = Math.min(Math.max(chara.size.width * .5, originalPos.x), this.playZone.width - chara.size.width * .5);
        // originalPos.y += deltaY * percent;
        // originalPos.y = Math.min(Math.max(chara.size.width * .5, originalPos.y), this.playZone.height - chara.size.width * .5);

        // Matter.Body.setPosition(chara.body, {x: originalPos.x, y: originalPos.y});

        // this.camLock.x = originalPos.x;
        // this.camLock.y = originalPos.y;
        //#endregion
    }

}