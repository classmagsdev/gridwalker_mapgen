
export class CustomCamera {

    camera: any;
    cameraObj: any;
    
    focusedObj: any;
    following: boolean;

    playZone: {width: 0, height: 0}

    constructor(root, camera, cameraObj, playZone) {
        this.camera = camera;
        this.cameraObj = cameraObj;
        this.focusedObj = undefined;
        this.following = false;

        this.playZone = playZone;
        root.addEventListener('tick', this.alignCamera.bind(this));
    }

    startFollowingObj(whichObj) {
        this.focusedObj = whichObj;
        this.following = true;
    }

    alignCamera() {
        if (this.focusedObj == undefined || !this.following) return;

        this.cameraObj.x = this.focusedObj.camLock.x; 
        this.cameraObj.y = this.focusedObj.camLock.y; 
        
        this.checkForBoundaries();
    }

    checkForBoundaries() {

        // keeps camera locked within playzone
        const stagewidth = 1024;
        const stageheight = 640;
        if (this.focusedObj.camLock.x < stagewidth / 2) {
            this.cameraObj.x = stagewidth / 2;
        } else if (this.focusedObj.camLock.x > this.playZone.width - (stagewidth / 2)) {
            this.cameraObj.x = this.playZone.width - (stagewidth / 2);
        }

        if (this.focusedObj.camLock.y < stageheight / 2) {
            this.cameraObj.y = stageheight / 2;
        } else if (this.focusedObj.camLock.y > this.playZone.height - (stageheight / 2)) {
            this.cameraObj.y = this.playZone.height - (stageheight / 2);
        }
        
        // any additional checks
    }

}