import { GridItem } from './GridItem';

export class Collectible extends GridItem {

    constructor(label, x, y, size) {
        super(label, x, y, size);
        
        this.type = "collectible";
    }

    // collectItem() {
    // }
}