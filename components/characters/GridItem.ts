
import * as Matter from 'matter-js';
import { OpenTile } from '../DungeonMaster';

export class GridItem {

    type: string;
    label: string;
    location: {x: number, y: number};
    size: {width: number, height: number}
    body: Matter.Physics.Body;
    occupyingTile: OpenTile;

    constructor(label, x, y, size) {
        this.type = "item";
        this.label = label;
        
        this.location = {x: x, y: y};
        this.size = {width: size.width, height: size.height};
    }

    setCurrentTile(newTile: OpenTile) {
        if (this.occupyingTile) {
            this.occupyingTile.setOccupied(false);
        }
        this.occupyingTile = newTile;
        newTile.setOccupied(true);
    }

    // findRandomLocation(emptySlots: Tile[]) {
    //     var newTile = emptySlots[Math.floor(Math.random() * emptySlots.length)];
    //     while (newTile.getOccupied) {
    //         // randomly choose tile until find an open tile that is not occupied
    //         newTile = emptySlots[Math.floor(Math.random() * emptySlots.length)];
    //     }
    //     this.setCurrentTile(newTile);
    // }

}