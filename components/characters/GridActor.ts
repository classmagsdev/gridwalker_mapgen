import * as Matter from 'matter-js';

export class GridActor {

    public type: string;
    public body: Matter.Physics.Body;
    public speed: number;
    public movement: any;
    size: {width: number, height: number};
    startingPos: {x: number, y: number}

    constructor(size) {
        this.speed = 0;
        this.size = {width: size.width, height: size.height};
    }

    setStartingPos(x: number, y: number) {
        this.startingPos = {x: x, y: y};
    }

    setSpeed(newSpeed: number) {
        this.speed = newSpeed; //speed is num of pixels travelled on tick
    }

    setMovement(movementHandler) {
        this.movement = movementHandler;
    }

    disableMovement() {
        this.movement.stopMove();
        
        // respective idle animation
    }

    enableMovement() {
        this.movement.startMove();
    }

}