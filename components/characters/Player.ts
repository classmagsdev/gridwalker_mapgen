
import { GridActor } from './GridActor';
// import { Movement_FollowMouse } from '../movement/Movement_FollowMouse'

export class Player extends GridActor {
    
    isInside: boolean;

    camLock: createjs.MovieClip;

    constructor(size) {

        super(size);

        this.speed = 20;
        this.type = "PLAYER";

    }

    
    createCamLock(root) {

        var newMC = new createjs.MovieClip();
        var newRect = new createjs.Shape(new createjs.Graphics().beginFill("#999999").drawRect(0, 0, this.size.width, this.size.height));

        newMC.addChild(newRect);
        newMC.children[0].x -= this.size.width / 2;
        newMC.children[0].y -= this.size.height / 2;
        newMC.x = this.startingPos.x;
        newMC.y = this.startingPos.y;

        root.camLock.addChild(newMC);

        this.camLock = newMC;
    }

}