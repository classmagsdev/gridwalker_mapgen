import * as Matter from 'matter-js';
import { InteractionsManager } from './InteractionsManager';
import { GridItem } from './characters/GridItem';
import { GridActor } from './characters/GridActor';
import { ColliderTile } from './DungeonMaster';

export class PhysicsManager {
    private static _instance: PhysicsManager;

    protected engine;
    protected world;
    protected runner;
    
    allBodyMCs: createjs.MovieClip[];

    interactions: InteractionsManager;

    constructor(interactionsManager = undefined) {

        this.allBodyMCs = [];

        var engine = Matter.Engine.create();
        var world = engine.world;
        var Bodies = Matter.Bodies;
        var World = Matter.World;
        var Events = Matter.Events;

        world.gravity.y = 0;
    
        var runner = Matter.Runner.create({ "delta": 60 });
        Matter.Runner.run(runner, engine);
    
    
        var lastDelta = createjs.Ticker.interval;
    
        this.engine = engine;
        this.world = world;
        this.runner = runner;

        this.addInteractions(interactionsManager);

        Events.on(engine, "collisionStart", this.collisionCheck.bind(this));

    }

    addInteractions(interactionsManager) {
        this.interactions = interactionsManager;
    }

    public static getInstance() {
        if (this._instance == null) {
            this._instance = new PhysicsManager();
        }

        return this._instance;
    }

    loadMapPhysics(pools) {

        if (pools.colliders) { this.generateColliderTiles(pools.colliders) };
        if (pools.actors) { this.generateActors(pools.actors); };
        if (pools.items) { this.generateItems(pools.items); };
        if (pools.indoorSlots) { this.generateIndoorTiles(pools.indoorSlots); };
        
    }

    generateBox(ref, x, y, w, h, options = {}, label = "") {
        //@ts-ignore
        if (options.isStatic) {
            x += w/2;
            y += w/2;
        }

        var newBoxBody = Matter.Bodies.rectangle(x, y, w, h, options);
        newBoxBody.label = label;

        newBoxBody.ref = ref;
        ref.body = newBoxBody;

        Matter.World.addBody(this.world, newBoxBody);

        return newBoxBody;
    }


    generateActors(actors) {

        for (var i in actors) {
            if (actors[i].type == "PLAYER") {
                this.generatePlayer(actors[i]);
            }
        }
    }

    generatePlayer(player) {
        var buffer = 5;
        this.generateBox(player, player.startingPos.x, player.startingPos.y, player.size.width - buffer, player.size.height - buffer, {}, "player");
    }

    generateColliderTiles(colliders) {
        for (var i in colliders) {
            var collider = colliders[i];
            // these are 1 full tile
            this.generateBox(collider, collider.location.x, collider.location.y, collider.size.width, collider.size.height, {isStatic: true}, collider.label);
        }
    }

    generateItems(items) {
        for (var i in items) {
            var item = items[i];
            var isSensor = item.type == "collectible" ? true : false;
            this.generateBox(item, item.location.x, item.location.y, item.size.width, item.size.height, {isStatic: true, isSensor: isSensor}, item.label);
        }
    }

    generateIndoorTiles(indoorTiles) {
        for (var i in indoorTiles) {
            var indTile = indoorTiles[i];
            this.generateBox(indTile, indTile.location.x, indTile.location.y, indTile.size.width, indTile.size.height, {isStatic: true, isSensor: true}, indTile.label);
        }
    }

    collisionCheck(evt) {
        for (var i in evt.pairs) {
            var aCollision = evt.pairs[i];
            if (aCollision.bodyA.label == "player" || aCollision.bodyB.label == "player") {
                this.interactions.handlePlayerThingInteraction(aCollision.bodyA, aCollision.bodyB);
            }
        }
    }


}