import * as Matter from 'matter-js';

export class InteractionsManager {
    private static _instance: InteractionsManager;

    constructor() {
    }

    public static getInstance() {
        if (this._instance == null) {
            this._instance = new InteractionsManager();
        }

        return this._instance;
    }

    handlePlayerThingInteraction(bodyA, bodyB) {
        var otherItem = (bodyA.label == "player") ? bodyB : bodyA;

        switch (otherItem.ref.type) {
            case "collectible":
                console.log("interacting with collectible", otherItem.label);
                //handle interaction
                break;
            case "indoor":
                console.log('stepped on indoor tile');
                //handle indoor
                break;
            case "enemy":
                console.log('collided with enemy');
                //handle enemy
                break;
            default:
                break;
        }
        
    }

}