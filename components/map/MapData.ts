import * as $ from 'jquery';

export class MapData {

    map: string[][];

    legend;

    constructor(json: string) {

        var myRef = this;

        $.getJSON(json, function(data) {
            console.log('JSON loaded');
            myRef.parseData(data);
        })
        
    }

    parseData(data) {
        this.map = data.map;
        this.legend = data.legend;
    }

}