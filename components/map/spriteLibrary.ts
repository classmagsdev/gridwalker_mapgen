
export class SpriteLibrary {
   
    root;
    library;

    constructor(root) {
        var library = {
            scene: {
                grass_0: root.library.grass_piece0,
                bush_0: root.library.bush_piece0,
                stone_wall_0: root.library.stonewall_piece0,
                stone_wall_1: root.library.stonewall_piece1,
                bridge_horizontal_0: root.library.bridge_horizontal0,
                bridge_vertical_0: root.library.bridge_vertical0,
                water_open_0: root.library.water_open0,
                water_edge_tl: root.library.water_e_tl,
                water_edge_tr: root.library.water_e_tr,
                water_edge_bl: root.library.water_e_bl,
                water_edge_br: root.library.water_e_br,
                water_inner_tl: root.library.water_i_tl,
                water_inner_tr: root.library.water_i_tr,
                water_inner_bl: root.library.water_i_bl,
                water_inner_br: root.library.water_i_br,
                water_edge_l: root.library.water_e_l,
                water_edge_r: root.library.water_e_r,
                water_edge_b: root.library.water_e_b,
                water_edge_t: root.library.water_e_t,
                dirt_open: root.library.dirt_open,
                dirt_edge_l: root.library.dirt_e_l,
                dirt_edge_r: root.library.dirt_e_r,
                dirt_edge_b: root.library.dirt_e_b,
                dirt_edge_t: root.library.dirt_e_t,
                dirt_edge_tl: root.library.dirt_e_tl,
                dirt_edge_tr: root.library.dirt_e_tr,
                dirt_edge_bl: root.library.dirt_e_bl,
                dirt_edge_br: root.library.dirt_e_br,
            },
            items: {
                potion: root.library.potionPiece,
                barrel: root.library.barrelPiece,
                door: root.library.door_stone0,
                archway: root.library.archway_stone0,
            },
            actors: {
                player: root.library.player,
                enemy0: root.library.enemy0
            }
            
        };


        this.root = root;
        this.library = library;

        this.removeAllFromStage();
    }

    removeAllFromStage() {
        for (var i in this.library) {
            for (var j in this.library[i]) {
                this.library[i][j].parent.removeChild(this.library[i][j]);
                this.library[i][j].visible = false;
            }
            
        }
    }

    getLibrary() {
        return this.library;
    }
}