//@ts-ignore
import MapData from './MapData';

export class SceneRenderer {
    private _render: createjs.Container;
    private _tileLayer: createjs.Container;
    private _actorLayer: createjs.Container;
    private _itemLayer: createjs.Container;
    // private _skyLayer: createjs.Container;

    private _library;
    private _actors: Actor[];
    private _items: Item[];

    constructor(library, mapData, root) {
        this.SetLibrary(library);
        this._actors = [];
        this._items = [];

        this._render = new createjs.Container();
        this._tileLayer = new createjs.Container();
        this._actorLayer = new createjs.Container();
        this._itemLayer = new createjs.Container();
        // _skyLayer = new createjs.Container();

        this._render.addChild(this._tileLayer);
        this._render.addChild(this._actorLayer);
        this._render.addChild(this._itemLayer);
        // this._render.addChild(this._skyLayer);

        this._render.addEventListener("tick", this.UpdatePositions.bind(this));
        
        this.CreateTileLayer(mapData, root);

        root.addChild(this._render);
    }

    public RenderInto(parent: createjs.Container) {
        parent.addChild(this._render);
    }

    public SetLibrary(library) {
        this._library = library;
    }

    public CreateTileLayer(mapData: MapData, root) {
        this._tileLayer.children = [];
        for (var i = 0; i < mapData.map.length; i++) {
            for (var j = 0; j < mapData.map[i].length; j++) {
                // instantiate tile sprite and add to _tileLayer
                
                var thisPiece;
                if (mapData.legend[mapData.map[i][j]]) {
                    thisPiece = mapData.legend[mapData.map[i][j]];
                } else {
                    thisPiece = mapData.legend["default"]
                }
                var mySprite = new this._library.scene[thisPiece.sprite].constructor();
                mySprite.type = thisPiece.type;

                var newLoc = {x: j * mySprite.getBounds().width, y: i * mySprite.getBounds().height};
                var size = {width: mySprite.getBounds().width, height: mySprite.getBounds().height};
                
                mySprite.x = newLoc.x;
                mySprite.y = newLoc.y;

                this._tileLayer.children.push(mySprite);

                if (thisPiece.item || thisPiece.collectible) {
                    var myItem = (thisPiece.item) ? new Item(thisPiece.item, new this._library.items[thisPiece.item].constructor()) : new Collectible(thisPiece.collectible, new this._library.items[thisPiece.collectible].constructor());
                    myItem.setStartingPos(newLoc.x, newLoc.y);
                    this._itemLayer.children.push(myItem.sprite);
                    this._items.push(myItem);
                    myItem.size = size;
                }
                

                if (thisPiece.actor) {
                    var myActor = new Actor(thisPiece.actor, new this._library.actors[thisPiece.actor].constructor());
                    myActor.setStartingPos(newLoc.x, newLoc.y);
                    this._actorLayer.children.push(myActor.sprite);
                    this._actors.push(myActor);
                    myActor.size = size;
                }

            }
        }
    }

    UpdatePositions() {
        if (this._render == undefined) return;


        for (var i = 0; i < this._actors.length; i++) {
            
            if (this._actors[i].bodyRef) {
                var actor = this._actors[i];
                actor.sprite.x = actor.bodyRef.position.x - (actor.size.width / 2);
                actor.sprite.y = actor.bodyRef.position.y - (actor.size.height / 2);
            }
        }

        for (var i = 0; i < this._items.length; i++) {
            if (this._items[i].bodyRef) {
                var item = this._items[i];
                item.sprite.x = item.bodyRef.position.x - (item.size.width / 2);
                item.sprite.y = item.bodyRef.position.y - (item.size.height / 2);
            }
        }

    }

}


export class Item {
    bodyRef;
    sprite: createjs.MovieClip;
    type: string;
    itemLabel: string;
    startingPos: { x: number, y: number };    
    size: {width: number, height: number};

    
    constructor(itemLabel, sprite) {
        if (sprite) {
            this.addSprite(sprite);
        }
        this.type = "item";
        this.itemLabel = itemLabel;
        this.startingPos = {x: 0, y: 0};
    }

    addSprite(sprite: createjs.MovieClip) {
        this.sprite = sprite;
    }
    
    setStartingPos(x, y) {
        //will be handled after by matching to physicsbody
        this.startingPos.x = x;
        this.startingPos.y = y;
        this.sprite.x = x;
        this.sprite.y = y;
    }
}


class Collectible extends Item {
    constructor(itemLabel, sprite = undefined) {
        super(itemLabel, sprite);
        this.type = "collectible";
    }
}

export class Actor {
    bodyRef;
    sprite: createjs.MovieClip;
    label: string;
    size: {width: number, height: number};

    constructor(label, sprite) {
        this.sprite = sprite;
        this.label = label;
    }

    setStartingPos(x, y) {
        this.sprite.x = x;
        this.sprite.y = y;
    }
}