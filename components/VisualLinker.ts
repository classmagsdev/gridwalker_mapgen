import { GridItem } from "./characters/GridItem";
import { GridActor } from "./characters/GridActor";
import { Actor } from "./map/SceneRenderer";
import { Item } from "./map/SceneRenderer";

// currently a sample class more for testing purposes

export class VisualLinker {
    gridActors: GridActor[] = [];
    gridItems: GridItem[] = [];
    actors: Actor[] = [];
    items: Item[] = [];
    
    constructor(gridActors: GridActor[], gridItems: GridItem[], actors: Actor[], items: Item[]) {
        for (var i in gridActors) {
            this.gridActors.push(gridActors[i]);
        }
        for (var i in gridItems) {
            this.gridItems.push(gridItems[i]);
        }
        for (var i in actors) {
            this.actors.push(actors[i]);
        }
        for (var i in items) {
            this.items.push(items[i]);
        }

        this.linkActors(this.gridActors, this.actors);
        this.linkItems(this.gridItems, this.items);
    }

    linkActors(gridActors: GridActor[], actors: Actor[]) {
        for (var i in gridActors) {
            let linked = false;
            for (var j = 0; j < actors.length; j++) {
                if (gridActors[i].body.label == actors[j].label) {
                    actors[j].bodyRef = gridActors[i].body;
                    actors.splice(j, 1);
                    linked = true;
                    break;
                }
            }
            if (!linked) {
                console.log("-- was unable to link ", gridActors[i].body.label);
            }
        }
    }

    linkItems(gridItems: GridItem[], items: Item[]) {
        for (var i in gridItems) {
            let linked = false;
            for (var j = 0; j < items.length; j++) {
                if (gridItems[i].body.label == items[j].itemLabel) {
                    items[j].bodyRef = gridItems[i].body;
                    items.splice(j, 1);
                    linked = true;
                    break;
                }
            }
            if (!linked) {
                console.log("-- was unable to link ", gridItems[i].body.label);
            }
        }
    }

}