import * as Matter from 'matter-js';
//@ts-ignore
import MapData from './map/MapData';
import { GridItem } from './characters/GridItem';
import { GridActor } from './characters/GridActor';
import { Player } from './characters/Player';
import { Collectible } from './characters/Collectible';

export class DungeonMaster {

    mapData: MapData;
    actors: GridActor[];
    items: GridItem[];
    openSlots: OpenTile[];
    colliders: ColliderTile[];
    indoorSlots: IndoorTile[];
    player: Player;

    root;

    mapSize: {width: number, height: number};
    blockSize: number;

    allTiles: Tile[][];

    constructor(root, mapData, blockSize) {
        this.root = root;
        this.mapData = mapData;
        this.mapSize = this.findMapSize();
        this.blockSize = blockSize;

        this.IngestMapData();
    }

    IngestMapData() {
        this.actors = [];
        this.items = [];
        this.colliders = [];
        this.openSlots = [];
        this.indoorSlots = [];
        this.allTiles = [];

        var size = {width: this.blockSize, height: this.blockSize};

        for (var i = 0; i < this.mapData.map.length; i++) {
            var tileRow = [];
            for (var j = 0; j < this.mapData.map[i].length; j++) {
                
                // Load Base Tiles
                var thisPiece;
                if (this.mapData.legend[this.mapData.map[i][j]]) {
                    thisPiece = this.mapData.legend[this.mapData.map[i][j]];
                } else {
                    thisPiece = this.mapData.legend["default"]
                }

                var newTile;
                if (thisPiece.type == "wall" || thisPiece.type == "bush" || thisPiece.type == "water") {
                    newTile = new ColliderTile(thisPiece.type, this.blockSize * j, this.blockSize * i, [i, j]);
                    this.colliders.push(newTile);
                } else {
                    if (thisPiece.type == "indoor") {
                        newTile = new IndoorTile(thisPiece.type, this.blockSize * j, this.blockSize * i, [i, j]);
                        this.indoorSlots.push(newTile);
                        this.openSlots.push(newTile);
                    } else {
                        newTile = new OpenTile(thisPiece.type, this.blockSize * j, this.blockSize * i, [i, j])
                        this.openSlots.push(newTile);
                    }
                    newTile.setOccupied((thisPiece.item || thisPiece.collectible) ? true : false);
                }
                
                // Load Item
                if (thisPiece.item) {
                    var newItem = new GridItem(thisPiece.item, this.blockSize * j, this.blockSize * i, size);
                    this.items.push(newItem);
                    newItem.setCurrentTile(newTile);
                    
                }
                if (thisPiece.collectible) {
                    var newItem = new Collectible(thisPiece.collectible, this.blockSize * j, this.blockSize * i, size);
                    this.items.push(newItem);
                    newItem.setCurrentTile(newTile);
                }

                // Load Actor
                if (thisPiece.actor) {
                    switch (thisPiece.actor) {
                        case "player": {
                            var myPlayer = new Player(size);
                            this.actors.push(myPlayer);
                            myPlayer.setStartingPos(this.blockSize * j + (this.blockSize / 2), this.blockSize * i + (this.blockSize / 2));
                            myPlayer.createCamLock(this.root);
                            this.player = myPlayer;
                            break;
                        }
                        default: {
                            console.log("this actor ", thisPiece.actor, " is not accounted for.");
                            break;
                        }
                    }
                    
                }


                tileRow.push(newTile);
            }

            this.allTiles.push(tileRow);
        }
    }

    findMapSize() {
        var height = this.mapData.map.length;
        var width = this.mapData.map[0].length
        for (var i in this.mapData.map) {
            if (this.mapData.map[i].length > width) {
                width = this.mapData.map[i].length;
            }
        }

        return {width: width, height: height};
    }

    getPhysicsDependentPools() {
        var pools = {
            actors: this.actors,
            items: this.items,
            indoorSlots: this.indoorSlots,
            colliders: this.colliders,
        }
        
        return pools;
    }
}

export class Tile {
    type: string;
    label: string;
    location: {x: number, y: number};
    index: [number, number];
    size: {width: number, height: number}
    body: Matter.Physics.Body;

    constructor(x: number, y: number, index) {
        this.type = "tile";
        this.location = {x: x, y: y};
        this.size = {width: 95, height: 95}; //todo check
    }
}


export class ColliderTile extends Tile {
    constructor(label: string, x: number, y: number, index) {
        super(x, y, index);
        this.type = "collider";
        this.label = label;
    }
}

export class OpenTile extends Tile {
    occupied: boolean;

    constructor(label: string, x: number, y: number, index) {
        super(x, y, index);
        this.type = "open";
        this.label = label;
    }

    getOccupied() {
        return this.occupied;
    }

    setOccupied(which: boolean) {
        this.occupied = which;
    }
}

export class IndoorTile extends OpenTile {
    constructor(label: string, x: number, y: number, index) {
        super(label, x, y, index);
        this.type = "indoor";
        this.label = label;
    }
}
