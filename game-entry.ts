/// <reference types="createjs" />
declare var AdobeAn: any;

import { DynamicContainer } from '@canvas-lib/src/utils/CoreUtilities'
import { CoreUtilities } from '@canvas-lib/src/utils/CoreUtilities'
import { MenuBarUtilities } from '@canvas-lib/src/utils/MenuBarUtilities';
import { Popup } from '@canvas-lib/src/ui/Popup';
import { Analytics } from '@canvas-lib/src/utils/Analytics';

import { CustomCamera } from './components/CustomCamera';
import { SpriteLibrary } from './components/map/spriteLibrary';
import { MapData } from './components/map/MapData';
import { SceneRenderer } from './components/map/SceneRenderer';
import { DungeonMaster } from './components/DungeonMaster';
import { VisualLinker } from './components/VisualLinker';
import { PhysicsManager } from './components/PhysicsManager';
import { Movement_FollowMouse } from './components/movement/Movement_FollowMouse';

import * as $ from 'jquery';
window.$ = $;
import * as cl from '@canvas-lib/static/canvas-lib';
import { InteractionsManager } from './components/InteractionsManager';
window.cl = cl;


window['Main'] = function Main(stage: createjs.Stage, lib: any, root: DynamicContainer) {

    //#region BOILERPLATE
    Analytics.LogEvent(Analytics.GameEventActions.GAME_LAUNCHED);

    //#region stage/library/root setup
    // adds touch support and ipad support (coordinate mapping for a rotated canvas)
    CoreUtilities.InitStage(stage);

    // makes instantiation from library available via instance.constructor();
    CoreUtilities.MapLibrary(lib, AdobeAn);

    // initializes the display list, assigns instance names to name property, assigns _parent property
    // CoreUtilities.InitializeDisplayObject(root);

    // standardizes textfield rendering across browsers
    CoreUtilities.CheckAllSubclipsRecursively(root, CoreUtilities.AdjustType, false);
    //#endregion

    //#region MENUBAR/POPUPS
    //#region CREDITS
    // MenuBarUtilities.AddCreditsButton();
    // let creditsPopup: Popup = new Popup(root.creditsPopup, Popup.PopupStatus.CLOSED);
    // window.addEventListener(MenuBarUtilities.CREDITS, ToggleCredits);
    // function ToggleCredits(evt) {
    //     console.log("ToggleCredits");
    //     if (creditsPopup.status == Popup.PopupStatus.CLOSED) {
    //         creditsPopup.OpenPopup();
    //     }
    //     else if (creditsPopup.status == Popup.PopupStatus.OPEN) {
    //         creditsPopup.ClosePopup();
    //     }
    // }
    //#endregion

    //#region SKILLS
    // MenuBarUtilities.AddSkillsButton();
    // let skillsPopup: Popup = new Popup(root.skillsPopup, Popup.PopupStatus.CLOSED);
    // window.addEventListener(MenuBarUtilities.SKILLS, ToggleSkills);
    // function ToggleSkills(evt) {
    //     console.log("ToggleSkills");
    //     if (skillsPopup.status == Popup.PopupStatus.CLOSED) {
    //         skillsPopup.OpenPopup();
    //     }
    //     else if (skillsPopup.status == Popup.PopupStatus.OPEN) {
    //         skillsPopup.ClosePopup();
    //     }
    // }
    //#endregion

    //#region DIRECTIONS
    // MenuBarUtilities.AddDirectionsButton();
    // window.addEventListener(MenuBarUtilities.DIRECTIONS, ToggleDirections);

    // let gameInProgress: boolean = false;
    // let directionsAudio: createjs.AbstractSoundInstance = createjs.Sound.createInstance("directions")
    // let directionsPopup: Popup = new Popup(root.directionsPopup, Popup.PopupStatus.CLOSED);
    // directionsPopup.mc.popup.savedPosition = { "x": directionsPopup.mc.popup.x, "y": directionsPopup.mc.popup.y }

    // // handle playBtn setup/listeners
    // if (directionsPopup.mc.popup.playBtn) {
    //     directionsPopup.mc.popup.playBtn.gotoAndPlay(0);
    //     directionsPopup.mc.popup.playBtn.mouseChildren = false;
    //     directionsPopup.mc.popup.playBtn.addEventListener('click', PlayButtonClicked);
    //     directionsPopup.mc.popup.playBtn.loop = -1;
    //     Object.defineProperty(directionsPopup.mc.popup.playBtn, 'loop', {
    //         get: function () { return -1 }
    //     });
    // }

    // function PlayButtonClicked() {
    //     directionsPopup.mc.popup.mouseEnabled = false;
    //     directionsPopup.mc.popup.playBtn.gotoAndStop(0);
    //     directionsPopup.mc.popup.playBtn.alpha = .5;

    //     if (directionsAudio.duration > 0) {
    //         createjs.Sound.stop();
    //         directionsAudio.play();
    //         directionsAudio.addEventListener("complete", DirectionsAudioComplete);
    //     }
    //     else {
    //         ToggleDirections();
    //     }
    // }

    // function ToggleDirections(evt = null) {
    //     console.log("ToggleDirections");
    //     if (createjs.Tween.hasActiveTweens(directionsPopup.mc.popup)) return;

    //     if (directionsPopup.status == Popup.PopupStatus.OPEN) {
    //         directionsPopup.mc.popup.mouseEnabled = false;
    //         directionsAudio.stop();
    //         DirectionsAudioCleanup();
    //         let zero: createjs.Point = directionsPopup.mc.globalToLocal(0, 0);
    //         createjs.Tween.get(directionsPopup.mc.blocker).to({ "alpha": .01 }, 750, createjs.Ease.get(-1));
    //         createjs.Tween.get(directionsPopup.mc.popup).to({ "x": zero.x, "y": zero.y, scale: 0.02 }, 750, createjs.Ease.get(-1)).call(directionsPopup.ClosePopup, [DirectionsPopupClosed], directionsPopup);
    //     }
    //     else {
    //         directionsPopup.OpenPopup();
    //         if (directionsPopup.mc.popup.playBtn) {
    //             directionsPopup.mc.popup.playBtn.alpha = 1;
    //             directionsPopup.mc.popup.playBtn.gotoAndPlay(0);
    //         }
    //         createjs.Tween.get(directionsPopup.mc.blocker).to({ "alpha": .6 }, 750, createjs.Ease.get(1));
    //         createjs.Tween.get(directionsPopup.mc.popup).to({ "x": directionsPopup.mc.popup.savedPosition.x, "y": directionsPopup.mc.popup.savedPosition.y, scale: 1 }, 750, createjs.Ease.get(1)).call(DirectionsPopupOpen);
    //     }
    // }

    // function DirectionsPopupOpen() {
    //     directionsPopup.mc.popup.mouseEnabled = true;
    // }

    // function DirectionsPopupClosed(): void {
    //     if (!gameInProgress) {
    //         CoreUtilities.FocusIframe();
    //         gameInProgress = true;
    //         StartGame();
    //     }
    // }

    // function DirectionsAudioComplete(evt) {
    //     DirectionsAudioCleanup();
    //     ToggleDirections();
    // }

    // function DirectionsAudioCleanup() {
    //     directionsAudio.removeEventListener("complete", DirectionsAudioComplete);
    // }
    //#endregion

    //#region DISCUSS
    // MenuBarUtilities.AddDiscussButton();
    // let discussPopup: Popup = new Popup(root.discussPopup, Popup.PopupStatus.CLOSED);
    // window.addEventListener(MenuBarUtilities.DISCUSS, ToggleDiscuss);
    // function ToggleDiscuss(evt) {
    //     console.log("ToggleDiscuss");
    //     if (discussPopup.status == Popup.PopupStatus.CLOSED) {
    //         discussPopup.OpenPopup();
    //     }
    //     else if (discussPopup.status == Popup.PopupStatus.OPEN) {
    //         discussPopup.ClosePopup();
    //     }
    // }
    //#endregion

    //#region SCREENSHOT
    // ADDS SCREENSHOT MENU BUTTON 
    // MenuBarUtilities.AddScreenshotButton();
    //#endregion
    //#endregion

    /*
    // DEBUG LOGGING CODE
    */

    enum LOG_LEVELS {
        NONE,
        ERROR,
        DEBUG,
        INFO,
    }
    let LOG_LEVEL = LOG_LEVELS.DEBUG;
    function Log(level: LOG_LEVELS, ...args) {
        level <= LOG_LEVEL ? console.log.apply(this, args) : null;
    }

    //#endregion

    /*
    // START GAME CODE
    */

    const blockSize = 95;

    function StartGame() {

    }

    var map0 = "./content/map0.json";
    var map1 = "./content/map1.json";
    
    // change MapData parameter to one of the above maps to show
    var myMap = new MapData(map1);

    var myLibrary = new SpriteLibrary(root);

    var mySceneRenderer; 
    var myDM;
    var playZone;
    var camera; 
    var visLinker;
    var myPM;

    setTimeout(loadGame, 50);

    function loadGame() {
        mySceneRenderer = new SceneRenderer(myLibrary.getLibrary(), myMap, root.holder);
        myDM = new DungeonMaster(root, myMap, blockSize);
        myPM = new PhysicsManager(new InteractionsManager());
        myPM.loadMapPhysics(myDM.getPhysicsDependentPools());
        playZone = {width: blockSize * myDM.mapSize.width, height: blockSize * myDM.mapSize.height}; 
        camera = new CustomCamera(root, AdobeAn.VirtualCamera.getCamera(root), AdobeAn.VirtualCamera.getCameraAsMovieClip(root), playZone);
        camera.startFollowingObj(myDM.player);
        myDM.player.setMovement(new Movement_FollowMouse({root: root, stage: stage}, playZone, myDM.player, root.mousehandler.mouseHandler));
    
        visLinker = new VisualLinker(myDM.actors, myDM.items, mySceneRenderer._actors, mySceneRenderer._items);
    }
    

}